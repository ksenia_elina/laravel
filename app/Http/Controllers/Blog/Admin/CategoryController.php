<?php

namespace App\Http\Controllers\Blog\Admin;

use Illuminate\Http\Request;
use App\Http\Requests\BlogCategoryUpdateRequest;
use App\Http\Requests\BlogCategoryCreateRequest;
use App\Http\Controllers\Blog\Controller;
use App\Models\BlogCtegory as BlogCategory;

class CategoryController extends BaseController
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //dd(__METHOD__);
        $dsd = BlogCategory::all();
        $paginator = BlogCategory::paginate(5);
        return View('Blog.admin.categories.index', compact('paginator'));
       // dd($dsd);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
       // dd(__METHOD__);
       $item = new BlogCategory();
      // dd($item);
       $categoryList = BlogCategory::all();
       
       return view('Blog.admin.categories.edit', compact('item', 'categoryList'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(BlogCategoryCreateRequest $request)
    {
               $request->flash();
       // $id = 111111;
        $data = $request->input();
        if(empty($data['slug']))
        {
            $data['slug'] = str_slug($data['title']);
        }
        //дописать
        $item = new BlogCategory($data);
        dd($item);
        $item->save();
        
        if($item)
        {
            return redirect()
                ->route('blog.admin.categories.edit', $item->id)
                ->with(['success' => 'Успешно сохранено']);
        } else{
           return back()//редирект назад
                ->withErrors(['msg' =>'Ошибка сохранения'])
                ->withInputs();
        }

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
        dd(__METHOD__);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
        //dd(__METHOD__);
        $item = BlogCategory::findOrFail($id);
        $categoryList = BlogCategory::all();
        
        return View('Blog.admin.categories.edit', compact('item', 'categoryList'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(BlogCategoryUpdateRequest $request, $id)
    {
        //валидацию тут нежелательно, лучше в реквестах
       /* $rules = [
            'title' => 'required|min:5|max:200',
            'slug'  => 'max:200',
            'description' => 'string|max:500|min:3',
            'parent_id'  => 'required|integer|exists:blog_ctegories,id',
        ];
      //  $validatedData = $this->validate($request, $rules);
       // $validatedData = $request->validate($rules);
       // dd($validatedData);
      /* $validator = \Validator::make($request->all(),$rules);
       $validatedData[] = $validator->passes();
     //   $validatedData[] = $validator->validate();
         $validatedData[] = $validator->valid();
          $validatedData[] = $validator->failed();
           $validatedData[] = $validator->errors();
            $validatedData[] = $validator->fails();
        //
        dd($validatedData);*/
        //dd(__METHOD__, $request->all(), $id);
        $request->flash();
       // $id = 111111;
        $item = BlogCategory::find($id);
        if(empty($item))
        {
            return back()
                ->withErrors("Запись id=[{$id}] не найдена")
               ->withInputs($request->all());
            //dd($id);
        }
        
        $data = $request->all();
        $result = $item->fill($data)->save();
        
        if($result)
        {
            return redirect()
                ->route('blog.admin.categories.edit', $item->id)
                ->with(['success' => 'Успешно сохранено']);
        } else{
           return back()//редирект назад
                ->withErrors('Ошибка сохранения')
                ->withInputs();
        }

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
        dd(__METHOD__);
    }
}
