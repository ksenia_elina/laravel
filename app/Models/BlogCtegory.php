<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class BlogCtegory extends Model
{
    use SoftDeletes;
    //без этого что-то не работает, нужно для fill, без этого не сохранится
    protected $fillable = ['slug', 'description', 'title', 'parent_id'];
}
