<?php

use Illuminate\Database\Seeder;

class UserTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $data = [
            [
                'name' => 'Without author',
                'email'=> 'author_unknowing@g.g',
                'password'=> bcrypt(str_random(16)),
            ],
                        [
                'name' => 'Author',
                'email'=> 'author@g.g',
                'password'=> bcrypt('123123'),
            ],
        ];
        
        DB::table('users')->insert($data);
    }
    
}
