

<?php $__env->startSection('content'); ?>
<div class="container">
    <div class = "row justify-content-center">
        <div class="col-md-12">
            <nav class="navbar navbar-toggleable-md navbar-light bg-faded">
                <a class="btn btn-primary" href="<?php echo e(route('blog.admin.categories.create')); ?>">Добавить</a>
            </nav>
            <div class="card">
                <div class="card-body">
                    <table class="table table-hover">
                        <thead>
                            <tr>
                                <th>#</th>
                                <th>Категория</th>
                                <th>Родитель</th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php $__currentLoopData = $paginator; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $item): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                <?php /** @var \App\Models\BlogCategory $item */ ?>
                                <tr>
                                    <td><?php echo e($item->id); ?></td>
                                    <td>
                                        <a href="<?php echo e(route('blog.admin.categories.edit',$item->id )); ?>">
                                            <?php echo e($item->title); ?>

                                        </a>
                                    </td>
                                    <td <?php if(in_array($item->parent_id, [0,1])): ?> style="color:#ccc" <?php endif; ?>>
                                        <?php echo e($item->parent_id); ?>


                                    </td>
                                </tr>
                            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                        </tbody>
                    </table>
                </div>
            </div>
         </div>
    </div>
    <?php if($paginator->total() > $paginator->count()): ?>
        <br>
        <div class="row justify-content-center">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-body">
                        <?php echo e($paginator->links()); ?>

                    </div>
                </div>
            </div>
        </div>
</div>
<?php endif; ?>
<?php $__env->stopSection(); ?>
<?php echo $__env->make('layouts.app', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>