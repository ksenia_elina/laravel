

<?php $__env->startSection('content'); ?>
    <?php
        /** \App\Models\BlogCtegory*/
    ?>
    <form method="POST" action="<?php echo e(route('blog.admin.categories.update', $item->id)); ?>">
    <?php echo e(csrf_field()); ?>

    <?php echo e(method_field('PATCH')); ?>

        <div class="container">
            <div class="row justify-content-center">
                <div class="col-md-8">
                    <?php echo $__env->make('Blog.admin.category.includes.item_edit_main_col', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
                </div>
                 <div class="col-md-3">
                    <?php echo $__env->make('Blog.admin.category.includes.item_edit_add_col', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
                </div>
                
            </div>
        </div>
    </form>
<?php $__env->stopSection(); ?>
<?php echo $__env->make('layouts.app', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>