

<?php $__env->startSection('content'); ?>
    <?php
        /** \App\Models\BlogCtegory*/
    ?>
    <form method="POST" action="<?php echo e(route('blog.admin.categories.update', $item->id)); ?>">
    <?php echo e(csrf_field()); ?>

    <?php echo e(method_field('PATCH')); ?>

        <div class="container">
            <?php
                /** @var \Illuminate\Support\ViewErrorBag $errors */ 
            ?>
            <?php if($errors->any()): ?>
                <div class="row justify-content-center">
                    <div class="col-md-11">
                        <div class="alert alert-danger" role="alert">
                            <button type="button" class="close" data-dissmiss="alert" aria-label="Close">
                                <span aria-hidden="true"></span>                            
                            </button>
                            <?php echo e($errors->first()); ?>

                        </div>
                    </div>
                </div>
            <?php endif; ?>
            <?php if(session('success')): ?>
                  <div class="row justify-content-center">
                    <div class="col-md-11">
                        <div class="alert alert-success" role="alert">
                            <button type="button" class="close" data-dissmiss="alert" aria-label="Close">
                                <span aria-hidden="true"></span>                            
                            </button>
                            <?php echo e(session('success')); ?>

                        </div>
                    </div>
                </div>
            <?php endif; ?>
        <div class="row justify-content-center">
                <div class="col-md-8">
                    <?php echo $__env->make('Blog.admin.categories.includes.item_edit_main_col', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
                </div>
                 <div class="col-md-3">
                    <?php echo $__env->make('Blog.admin.categories.includes.item_edit_add_col', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
                </div>
                
            </div>
        </div>
    </form>
<?php $__env->stopSection(); ?>
<?php echo $__env->make('layouts.app', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>